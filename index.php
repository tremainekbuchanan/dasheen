<!DOCTYPE html>
<html>
  <head>
    <title>Dasheen Restaurant</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href='http://fonts.googleapis.com/css?family=Signika:400,300' rel='stylesheet' type='text/css'>
    <link href="bs/css/bootstrap.css" rel="stylesheet">
    <link href="bs/css/bootstrap-responsive.css" rel="stylesheet">
        
     <style>
      body {
            padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    
    </head>
  <body>
    

    <div class="container"> <!--Start of container -->

       <ul class="nav nav-pills pull-right" id="navbar">
        <li><a href='index.php'><i class="icon-home"></i>Home</a></li>
        <li><a href="https://www.facebook.com/DasheenRestaurant" target="_blank"><i class="icon-thumbs-up"></i>Like Us on Facebook</a></li>
        <li><a href="downloads/userguide.pdf"><i class="icon-print"></i>Download Menu</a></li>
        <li><a href="#" id="location"><i class="icon-map-marker"></i>Restaurant Location</a></li>
        <li><a href="#" id="contact"><i class="icon-envelope"></i>Contact Us</a></li> 
      </ul>

      <ul class="thumbnails">
          <li class="span4">
            <img src="http://placehold.it/600x200">
          </li>
      </ul>

     
      <!-- <h1>Dasheen Restaurant</h1>  -->    
      <?php 
        include ('carousel.php');
      ?>
      <div id="openhrs">
      <h4 class="menulabel">We are open Mondays-Fridays 7am - 3pm</h4>
      </div>
      <div id="mapTitle"></div>
      <div class="wellmod"> <!--Start of main well -->
           
        <?php include('navbar'); ?> 

        <ul class="nav nav-tabs" id="myTab">

        <li class="active"><a href="#breakfast" data-toggle="tab"><h4 class="menulabel">Breakfast Menu</h4></a></li>
        <li><a href="#lunch" data-toggle="tab"><h4 class="menulabel">Lunch Menu</h4></a></li>
        <li><a href="#sideorders" data-toggle="tab"><h4 class="menulabel">Salads</h4></a></li>
        <li><a href="#messages" data-toggle="tab"><h4 class="menulabel">Beverages</h4></a></li>
        <li><a href="#settings" data-toggle="tab"><h4 class="menulabel">Side Orders</h4></a></li>
        <li><a href="#desert" data-toggle="tab"><h4 class="menulabel">Deserts</h4></a></li>
        </ul>

        <div class="tab-content" id="tab-content">
        
        <div class="tab-pane active" id="breakfast"></div>
        <div class="tab-pane" id="lunch"></div>
        <div class="tab-pane" id="sideorders"></div>
        <div class="tab-pane" id="messages"></div>
        <div class="tab-pane" id="settings"></div>
        <div class="tab-pane" id="dessert"></div>
        </div>

        <div id="map">

        </div>
        <!--<h1>Today's Menu</h1> 
        <hr> -->
        <!--div class="well"> Inner well start -->
        

        <hr>
        <!--</div> Inner well end -->
        <?php include('footer.php'); ?>

        <div>
      </div> <!--End of main well -->

    
  </div><!--End of container -->
  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script type="text/javascript" src = "bs/js/bootstrap.js"></script>
  <script type="text/javascript" src = "scripts.js"></script>
  
  </body>

</html>
